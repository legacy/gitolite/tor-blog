<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Tor and the BEAST SSL attack</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Tor and the BEAST SSL attack</h2>
<p class="meta">24 Sep 2011</p>

<div class="post">
<p>Today, Juliano Rizzo and Thai Duong presented a new attack on TLS Ekoparty security conference in Buenos Aires. Let&#39;s talk about how it works, and how it relates to the Tor protocol.</p>

<p>Short version: Don&#39;t panic. The Tor software itself is just fine, and the free-software browser vendors look like they&#39;re responding well and quickly. I&#39;ll be talking about why Tor is fine; I&#39;ll bet that the TBB folks will have more to say about browsers sometime soon.</p>

<p>There is some discussion of the attack and responses to it out there already, written by <a href="http://www.schneier.com/blog/archives/2011/09/man-in-the-midd_4.html">seriously smart cryptographers</a> and <a href="http://www.imperialviolet.org/2011/09/23/chromeandbeast.html">high-test browser security people</a>. But I haven&#39;t seen anything out there yet that tries to explain what&#39;s going on for people who don&#39;t know TLS internals and CBC basics.</p>

<p>So I&#39;ll do my best. This blog post also assumes that <em>I</em> understand the attack. Please bear with me if I&#39;m wrong about that.</p>

<p>Thanks to the authors of the paper for letting me read it and show it to other Tor devs. Thanks also to Ralf-Philipp Weinmann for helping me figure the analysis out.</p>

<h2>The attack</h2>

<h3>How the attack works: Basic background</h3>

<p>This writeup assumes that you know a little bit of computer stuff, and you know how xor works.</p>

<p>Let&#39;s talk about block ciphers, for starters. A <a href="http://en.wikipedia.org/wiki/Block_cipher">block cipher</a> is a cryptographic tool that encrypts a small chunk of plaintext data into a same-sized chunk of encrypted data, based on a secret key.</p>

<p>In practice, you want to encrypt more than just one chunk of data with your block cipher at a time. What do you do if you have a block cipher with a 16-byte block (like AES), when you need to encrypt a 256-byte message? When most folks first consider this problem, they say something like &quot;Just split the message into 16-byte chunks, and encrypt each one of those.&quot; That&#39;s an old idea (it&#39;s called ECB, or &quot; <a href="http://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Electronic_codebook_.28ECB.29">electronic codebook</a>&quot;), but it has some serious problems. Most significantly, if the same 16-byte block appears multiple times in the plaintext, the ciphertext will also have identical blocks in the same position. The Wikipedia link above has a cute demonstration of this.</p>

<p>Okay, so nobody reasonable uses ECB. Some people, though, do use a mode called CBC, or &quot; <a href="http://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Cipher-block_chaining_.28CBC.29">Cipher Block Chaining</a>.&quot; With CBC, when you want to encrypt a message, you start your ciphertext message with a single extra random block, or IV (&quot;initialization vector&quot;). Now, when you go to encrypt each plaintext block to get its ciphertext block, you first <a href="http://en.wikipedia.org/wiki/Xor">xor</a> the plaintext with the previous ciphertext. (So you xor the IV with the first plaintext block, encrypt that, and output it. Then you xor that ciphertext block with the second plaintext block, encrypt that, output it, and so on. The Wikipedia page has pretty good examples and illustrations here too.)</p>

<p>TLS and its earlier incarnation, SSL, are the encryption protocols that many applications, including Tor, use to send streams of encrypted data. They use CBC mode for most of their block ciphers. Unfortunately, before TLS version 1.1, they made a bad mistake. Instead of using a new random IV for every TLS message they sent, they used the ciphertext of the last block of the last message as the IV for the next message.</p>

<p>Here&#39;s why that&#39;s bad. The IV is not just supposed to be random-looking; it also needs to be something that an attacker cannot predict. If I know that you are going to use IV x for your next message, and I can trick you into sending a message that starts with a plaintext block of [(NOT x) xor y], then you will encrypt y for me.</p>

<p>That doesn&#39;t sound too bad. But <a href="http://www.mail-archive.com/openssl-dev@openssl.org/msg10664.html">Wei Dai</a> and <a href="http://eprint.iacr.org/2004/111.pdf">Gregory V. Bard</a> both found ways to exploit this to learn whether a given ciphertext block corresponds to a given plaintext. The attacker makes the user encrypt C&#39; xor p, where p is the guessed plaintext and C&#39; is the ciphertext block right before where the attacker thinks that plaintext was. If the attacker guessed right, then the user&#39;s SSL implementation outputs the same ciphertext as it did when it first encrypted that block.</p>

<p>And even <strong>that</strong> doesn&#39;t sound too bad, even in retrospect. In order to mount this attack, an adversary would need to be watching your internet connection, and be able to force you to start your next TLS record with a given string of his choice, and be able to guess something sensitive that you said earlier, and guess which part of your TLS stream might have corresponded to it.</p>

<p>Nevertheless, crypto people implemented workarounds. Better safe than sorry! In version 1.1 of the TLS protocol, every record gets a fresh IV, so the attacker can&#39;t know the IV of the next message in advance. And OpenSSL implemented a fix where, whenever they&#39;re about to send a TLS record, they send an empty TLS record immediately before, and then send the record with the message in it. The empty TLS record is enough to make the CBC state change, effectively giving the real message a new IV that the attacker can&#39;t predict.</p>

<p>These fixes haven&#39;t gotten very far in the web world, though. TLS 1.1 isn&#39;t widely implemented or deployed, even though the standard has been out since 2006. And<br>
OpenSSL&#39;s &quot;empty record&quot; trick turns out to break some non-conformant SSL implementations, so lots of folks turn it off. (The OpenSSL manual page for the option in question even says that &quot;it is usually safe&quot; to do so.)</p>

<p>I suppose that, at the time, this seemed pretty reasonable. Guessing a plaintext is hard: there are something like 3.4 x 10^38 possible values. But as they say, attacks only get better.</p>

<h3>How the attack works: What&#39;s new as of today</h3>

<p>Juliano Rizzo and Thai Duong have two contributions, as I see it. (And please correct me if I&#39;m getting this wrong; I have not read the literature closely!) First, they came up with a scenario to implement a pretty clever variation of Dai&#39;s original attack.</p>

<p>Here&#39;s a simplified version, not exactly as Rizzo and Duong present it. Let&#39;s suppose that, for some reason, the user has a secret (like a web cookie) that they send in every TLS record. And let&#39;s assume also that the attacker can trick the user into inserting any number of characters in their plaintext at the start of the record right before the secret. So the plaintext for every record is &quot;Evil | Secret&quot;, where Evil is what the attacker chooses, and Secret is the secret message. Finally, let&#39;s suppose that the attacker can make the user send as many records as he wants.</p>

<p>The ability to decide where block boundaries fall turns out to be a big deal. Let&#39;s suppose that instead of filling up a full plaintext block with 16 bytes of Evil, the attacker only fills up 15 bytes... so the block will have 15 bytes the attacker controls, and one byte of the secret.</p>

<p>Whoops! There are only 256 possible values for a single byte, so the attacker can guess each one in turn, and use the older guess-checking attack to see if he guessed right. And once the attacker knows the first byte, he starts sending records with 14 attacker-controlled bytes, one byte that he knows (because he made a bunch of guesses and used the older attack to confirm which was right), and one byte that he doesn&#39;t. Again, this block has only 256 possible values, and so guessing each one in turn is efficient.</p>

<p>They then show how to extend this to cases where the attacker doesn&#39;t actually control the start of the block, but happens to know (or is able to guess) it, and make other extensions to the attack.</p>

<p>The second neat facet of Duong and Rizzo&#39;s work is that they actually found some ways to make this attack work against web browsers. That&#39;s no mean feat! You need to find a way to trick a web client into sending requests where you control enough of the right parts of them to mount this attack, and you need to be able to do it repeatedly. It seems to have taken a lot of HTTP hackery, but it seems they managed to do it. There&#39;s more detailed information here at <a href="http://www.educatedguesswork.org/2011/09/security_impact_of_the_rizzodu.html">Eric Rescorla&#39;s blog</a>, and of course once Juliano and Thai have their paper out, there will be even more to goggle at. This is really clever stuff IMO.</p>

<p>[Disclaimer: Again, I may have made mistakes above; if so, I will correct it when I wake up in the morning, or sooner. Please double-check me if you know this work better than I do.]</p>

<h2>So, does this attack work on Tor?</h2>

<p>Nope.</p>

<p>Tor uses OpenSSL&#39;s &quot;empty fragment&quot; feature, which inserts a single empty TLS record before every record it sends. This effectively randomizes the IV of the actual records, like a low-budget TLS 1.1. So the attack is simply stopped.</p>

<p>This feature has been in OpenSSL since 0.9.6d: see item 2 in <a href="http://www.openssl.org/%7Ebodo/tls-cbc.txt">Bodo Müller&#39;s good old CBC writeup</a> for full details on how it works. It makes our SSL incompatible with some standards-non-compliant TLS implementations... but we don&#39;t really care there, since all of the TLS implementations that connect to the Tor network are OpenSSL, or are compatible with it.</p>

<p>Tor requires OpenSSL 0.9.7 or later, and has since 0.2.0.10-alpha. Amusingly, we were considering dropping our use of the empty fragment feature as &quot;probably unnecessary&quot; in 2008, but we never got around to it. I sure don&#39;t think we&#39;ll be doing that now!</p>

<p>Now, it&#39;s possible that clients we didn&#39;t write might be using other TLS implementations, but the opportunity for plaintext injection on client-&gt;relay links is much lower than on relay-&gt;relay links; see below. As far as I know, there are no Tor server implementations compatible with the current network other than ours.</p>

<p>Also, this only goes for the Tor software itself. Applications that use TLS need to watch out. Please install patches, and look for new releases if any are coming out soon.</p>

<h2>But what if...</h2>

<h3>Okay, but would it work if we didn&#39;t use OpenSSL&#39;s empty-fragment trick?</h3>

<p>For fun, what would our situation be if we weren&#39;t using openssl&#39;s empty fragment trick?</p>

<p>I&#39;m going to diverge into Tor protocol geekery here. <strong>All of the next several sections are probably irrelevant</strong> , since the OpenSSL trick above protects Tor&#39;s TLS usage already. I&#39;m just into analyzing stuff sometimes... and at the time I originally wrote this analysis, we didn&#39;t have confirmation about whether the OpenSSL empty-record trick would help or not.</p>

<p>This part will probably be a little harder to follow, and will require some knowledge of Tor internals. You can find all of our documents and specifications online at our handy <a href="https://www.torproject.org/docs/documentation.html#DesignDoc">documentation page</a> if you&#39;ve got some free time and you want to come up to speed.</p>

<p>The attack scenario that makes sense is for an attacker to be trying to decrypt stuff sent from one Tor node to another, or between a Tor node and a client. The attacker is not one of the parties on the TLS link, obviously: if they were, they&#39;d already know the plaintext and would not need to decrypt it.</p>

<p>First, let&#39;s make some assumptions to make things as easy for the attacker as possible. Let&#39;s assume that the attacker can trivially inject chosen plaintext, and can have the TLS records carve up the plaintext stream anywhere he wants.</p>

<p>(Are those assumptions reasonable? Well, it&#39;s easy to inject plaintext on a relay-&gt;relay link: sending a node an EXTEND cell will make it send any CREATE cell body that you choose, and if you have built a circuit through a node, you can cause a RELAY cell on that node to have nearly any body you want, since the body is encrypted/decrypted in counter mode. I don&#39;t see an obvious way to make a node send a chosen plaintext to a client or make a client send a chosen plaintext to a node, but let&#39;s pretend that it&#39;s easy to do that too.</p>

<p>The second assumption, about how it&#39;s easy to make the boundaries of TLS records fall wherever you want, is likely to be more contentious; see &quot;More challenges for the attacker&quot; below.)</p>

<p>Also let&#39;s assume that on the targeted link, traffic for only one client is sent. An active attacker might arrange this through a trickle attack or something.</p>

<p>I am assuming that the attacker is observing ciphertext at a targeted node or client, but not at two targeted places that allow him to see the same traffic enter and leave the network: by our threat model, any attacker who can observe two points on a circuit is assumed to win via traffic correlation attacks.</p>

<p>I am going to argue that even then, the earlier attack doesn&#39;t get the attacker anything, and the preconditions for the Duong/Rizzo attack don&#39;t exist.</p>

<h3>CLAIM 1: The earlier (Dai/Bard) attacks don&#39;t get the attacker anything against the Tor protocol</h3>

<p>There are no interesting guessable plaintexts sent by a well-behaved client or node[*] on a Tor TLS link: all are either random, nearly random, or boring from an attacker&#39;s POV.</p>

<p>[*] What if a node or client is hostile? Then it might as well just publish its plaintext straight to the attacker.</p>

<p>Argument: CREATE cell bodies contain hybrid-encrypted stuff that (except for its first bit) should be indistinguishable from random bits, or pretty close to indistinguishable from random bits. CREATED cell bodies have a DH public key and a hash: also close to indistinguishable from random. CREATE_FAST cell bodies *are* randomly generated, and CREATED_FAST cell bodies have a random value and a hash.</p>

<p>RELAY and RELAY_EARLY cell bodies are encrypted with at least one layer of AES_CTR, so they shouldn&#39;t be distinguishable from random bits either.</p>

<p>DESTROY, PADDING, NETINFO, and VERSIONS cells do not have interesting bodies. DESTROY and PADDING bodies are either 0s or random bytes, and NETINFO and VERSIONS provide only trivial information that you can learn just by connecting to a node (the time of day, its addresses, and which versions of the Tor link protocol it speaks).</p>

<p>That&#39;s cell bodies. What about their headers? A Tor cell&#39;s header has a command and a circuit ID that take up only 3 bytes. Learning the command doesn&#39;t tell you anything you couldn&#39;t notice by observing the encrypted link in the first place and doing a little traffic analysis. The link-local 2-byte circuit ID is random, and not<br>
interesting per se, but possibly interesting if you could use it to demultiplex traffic sent over multiple circuits. I&#39;ll discuss that more below at the end of the next section.</p>

<h3>CLAIM 2: You can&#39;t do the Duong/Rizzo attack against the Tor protocol either</h3>

<p>The attack requires that some piece of sensitive information M that you want to guess be re-sent repeatedly after the attacker&#39;s chosen plaintext. That is, it isn&#39;t enough to get the target to send one TLS record containing (evil | M) -- you need to get it to send a bunch of (evil | M) records with the same M to learn much about M.</p>

<p>But in Tor&#39;s link protocol, nothing sensitive is sent more than once. Tor does not retry the same CREATE or CREATE_FAST cells, or re-send CREATED or CREATED_FAST cells. RELAY cells are encrypted with at least one layer of AES_CTR, and no RELAY cell body is sent more than once. (The same applies to RELAY_EARLY.)</p>

<p>PADDING cells have no interesting content to learn. VERSIONS and NETINFO cells are sent only at the start of the TLS connection (and their contents are boring too).</p>

<p>What about the cell headers? They contain a command and a circuit ID. If a node is working normally, you can already predict that most of the commands will be RELAY. You can&#39;t predict that any given circuit&#39;s cells will be sent reliably after yours, so you can&#39;t be sure that you&#39;ll see the same circuitID over and over... unless if you&#39;ve done a trickle attack, in which case you already know that all the cells you&#39;re seeing are coming from the same place; or if you&#39;ve lucked out and one circuit is way louder than all the others, but you would already learn that from watching the node&#39;s network. So I think that anything that is sent frequently enough to be decryptable with this method is not in fact worth decrypting. But see the next section.</p>

<h3>Hey, what if I&#39;m wrong about those cell headers?</h3>

<p>Let&#39;s pretend that I flubbed the analyis above, and that the attacker can read the command and circuitID for every cell on a link. All this allows the attacker to do is to demultiplex the circuits on the link, and better separate the traffic pattern flows that the link is multiplexing for different clients... but the attacker can&#39;t really<br>
use this info unless the attacker can correlate it with a flow somewhere else. This requires that the attacker be watching the same traffic at two points. But if the attacker can do that, the attacker can already (we assume) do a passive correlation attack and win.</p>

<h3>And what if I&#39;m wrong entirely?</h3>

<p>Now let&#39;s imagine that I am totally wrong, and TLS is completely broken, providing no confidentiality whatsoever? (Assume that it&#39;s still providing authenticity.)</p>

<p>Of course, this is a <em>really unlikely scenario</em>, but it&#39;s neat to speculate.</p>

<p>The attacker can remove at most one layer of encryption from RELAY cells in this case, because every hop of a circuit (except sometimes the first) is created with a one-way-authenticated Diffie-Hellman handshake. (The first hop may be created with a CREATE_FAST handshake, which is not remotely secure against an attacker who can see the plaintext inside the TLS stream.) Anonymized RELAY traffic is encrypted in AES_CTR mode with keys based on at least one CREATE handshake, so the attacker can&#39;t beat it. Non-anonymized RELAY traffic (that is, tunneled directory connections) don&#39;t contain sensitive requests or information.</p>

<p>So if the attacker can use this attack to completely decrypt TLS at a single hop on a Tor circuit, they still don&#39;t win. (All they can do is demultiplex circuits, about which see above.) And for them to do this attack at multiple points on the circuit, they would have to observe those points, in which case they&#39;re already winning according to our threat model.</p>

<h3>More challenges for the attacker</h3>

<p>It&#39;s actually even a little harder to do this attack against Tor than I&#39;ve suggested.</p>

<p>First, consider bandwidth-shaping: If a node or a link is near its bandwidth limit, it will split cells in order to stay under that limit.</p>

<p>Also, on a busy node, you can&#39;t really send a cell and expect it to get decrypted and put into the next output record on a given link: there are likely to be other circuits queueing other cells for that link, and by the time your cell is sent, it&#39;s likely that they&#39;ll have sent stuff too. You can get increased priority by making a very quiet<br>
circuit, though.</p>

<p>Finally, I don&#39;t actually think it&#39;s possible to cause chosen plaintext to appear on a client-&gt;server link.</p>

<h3>Recommendations</h3>

<p>We dodged an interesting bullet on this one: part of it was luck, and part of it was a redundant protocol design. Nevertheless, let&#39;s &quot;close the barn doors,&quot; even though the horse is still tied up, chained down, and probably sedated too.</p>

<p>First, as soon as OpenSSL 1.0.1 is out and stable, we should strongly suggest that people move to it. (It provides TLS 1.1.) We should probably build all of our packages to use it. But sadly, we&#39;ll have to think about protocol fingerprinting issues there, in case nobody else jumps onto the TLS 1.1 bandwagon with us. :/</p>

<p>We should include recommended use of the OpenSSL empty-record trick in our spec as a requirement or a strong recommendation. I&#39;ll add a note to that effect.</p>

<p>In future designs of replacements for the current RELAY and CREATE format, we should see if there are ways to prevent them from being used for chosen plaintext injection. This might not be the last attack of its kind, after all.</p>

<p>Perhaps we should put an upper bound on the circuit priority algorithm if we haven&#39;t already, so that no circuit can achieve more than a given amount of priority for being quiet, and so that you can&#39;t reliably know that your cell will be sent next on a link. But that&#39;s pretty farfetched.</p>

<h3>And in closing</h3>

<p><a href="http://www.google.com/search?q=longcat&amp;hl=en&amp;prmd=imvns&amp;tbm=isch">longpost is loooooooong</a></p>

<p>Thanks to everybody who followed along with me so far, thanks to Juliano and Thai for letting me read their paper ahead of time, and thanks to everybody who helped me write this up. And thanks especially to my lovely spouse for proofreading.</p>

<p>Now, it&#39;s a Friday evening, and if you&#39;ll excuse me, I think I&#39;m going to go off the &#39;nets for a little while.</p>

</div>
<p>- nickm</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
