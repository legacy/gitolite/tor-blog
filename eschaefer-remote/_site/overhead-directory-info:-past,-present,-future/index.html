<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Overhead from directory info: past, present, future</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Overhead from directory info: past, present, future</h2>
<p class="meta">16 Feb 2009</p>

<div class="post">
<p>A growing number of people want to use Tor in low-bandwidth contexts (e.g. modems or shared Internet cafes in the Middle East) and mobile contexts (start up a Tor client, use it for a short time, and then stop it again). Currently Tor is nearly unusable in these situations, because it spends too many bytes fetching directory info. This post summarizes the steps we&#39;ve taken so far to reduce directory overhead, and explains the steps that are coming next.</p>

<p>First, what do I mean by &quot;directory info&quot;? Part of the Tor design is the _discovery_ component: how clients learn about the available Tor relays, along with their keys, locations, exit policies, and so on. Tor&#39;s solution so far uses a few trusted directory authorities that sign and distribute official lists of the relays that make up the Tor network.</p>

<p><strong>History of v1, v2, v3 dir protocols</strong></p>

<p>Over the years we&#39;ve had several different &quot;directory protocols&quot;, each more bandwidth-friendly than the last, and often providing stronger security properties as well. In Tor&#39;s <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/attic/dir-spec-v1.txt">first directory design</a> (Sept 2002), each authority created its own list of every relay descriptor, as one flat text file. A short summary of relay status at the top of the file told clients which relays were reachable. Every Tor client fetched a copy from an authority every 10 minutes.</p>

<p>Tor 0.0.8 (Aug 2004) introduced &quot;directory caches&quot;, where normal relays would fetch a copy of the directory and serve it to others, to take some burden off the authorities. Tor 0.0.9 (Dec 2004) let clients download &quot;running routers&quot; status summaries separately from the main directory, so they could keep more up-to-date on reachability without needing to refetch all the descriptors. It also added zlib-style compression during transfer. At this point everybody fetched the whole directory every hour and the running-routers document every 15 minutes.</p>

<p>There were two big flaws with the v1 directory scheme: a security problem and an overhead problem. The security problem was that even though there were three authorities, you just went with the most recent opinion you could find, so a single evil authority could screw everybody. The overhead problem was that clients were fetching a new directory even when very little of it had changed. In Dec 2004 there were 57 relays and the uncompressed directory was 172KB; but by May 2006 we were up to 749 relays and the full directory was almost 1MB compressed. Even though we&#39;d lowered the period for fetching new copies to 2 hours (20 minutes for caches), this was not good.</p>

<p>We introduced the <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/dir-spec-v2.txt">v2 directory design</a> in Tor 0.1.1.20 in May 2006. Each authority now produced its own &quot;network status&quot; document, which listed brief summaries of each relay along with a hash of the current relay descriptor. Clients fetched all the status documents (there were 5 authorities by now) and ignored relays listed by less than half of them. Clients only fetched the relay descriptors they were missing. Once bootstrapped, clients fetched one new status document (round robin) per hour. Peter Palfrader produced a <a href="http://asteria.noreply.org/%7Eweasel/Tor/tor-client-download-stats-longterm-dl.jpg">graph of bandwidth needed to bootstrap and then keep up-to-date over a day</a>.</p>

<p>All of the improvements so far were oriented toward saving bandwidth at the server side: we figured that clients had plenty of bandwidth, and we wanted to avoid overloading the authorities and caches. But if we wanted to add more directory authorities (a majority of 5 is still an uncomfortably small number), bootstrapping clients would have to fetch one more network status for every new authority. By early 2008, each status document listed 2500 relay summaries and came in around 175KB compressed, meaning you needed 875KB of status docs when starting up, and then another megabyte of descriptors after that. And we couldn&#39;t add more authorities without making the problem even worse.</p>

<p>The <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/dir-spec.txt">v3 directory design</a> in Tor 0.2.0.30 (Jul 2008) solved this last problem by having the authorities coordinate and produce an hourly &quot;consensus&quot; network status document, signed by a majority of the six v3 authorities. Now clients only have to fetch one status document.</p>

<p><strong>Next fixes</strong></p>

<p>Based in part on sponsorship by <a href="http://www.nlnet.nl/">NLnet</a> to reduce Tor&#39;s directory overhead, there are two directions we&#39;re exploring now: reducing relay download overhead and reducing consensus networkstatus download overhead.</p>

<p>We&#39;ve cut relay descriptor size by 60% by <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/104-short-descriptors.txt">moving some of the descriptor info to separate &quot;extra-info&quot; descriptors that clients don&#39;t need to fetch</a>, and we&#39;ve cut the consensus size by 40% by <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/138-remove-down-routers-from-consensus.txt">leaving out non-Running relays</a> (since clients won&#39;t fetch descriptors for them anyway).</p>

<p>We spent the second half of 2008 working on a <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/141-jit-sd-downloads.txt">much more radical design change</a> where we move all the descriptor components that are used for path selection into the consensus, and then clients would download each relay descriptor &quot;just in time&quot; as part of circuit extension. This design would have two huge benefits: a) clients download zero descriptors at startup, greatly speeding bootstrapping, and b) the total number of relay descriptor downloads would be based on the number of circuits built, regardless of how many relays are in the network.</p>

<p>Alas, we have backed off on this proposal: Karsten Loesing&#39;s work on hidden service performance concluded that the circuit extension is the main performance killer, and our &quot;just in time&quot; download proposal would add more round-trips and complexity into exactly that step. So we have a new plan: we&#39;re still going to move all the path-selection information into the consensus, but then we&#39;re going to put the remaining pieces into a new <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/158-microdescriptors.txt">microdescriptor</a> that will hopefully change at most weekly. That means the initial bootstrap costs will still be there (though the microdescriptor is maybe a third the size of the normal descriptor), but so long as you can keep a disk cache, descriptor maintenance will be reduced to roughly zero. We&#39;re aiming to roll this change out in Tor 0.2.3.x, in late 2010.</p>

<p>We also have plans to further reduce the consensus download overhead. Since the consensus doesn&#39;t actually change that much from one hour to the next, clients should fetch <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/140-consensus-diffs.txt">consensus diffs</a> rather than fetching a whole new consensus. We could expect another 80% reduction in size here. We hope to roll this step out in mid to late 2009. Alas, this goal is stymied by the fact that <a href="http://archives.seul.org/or/dev/Jun-2008/msg00031.html">we haven&#39;t found any small portable BSD-licensed C diff libraries</a>. Anybody know one?</p>

<p>So these two changes together mean an initial bootstrap cost of maybe 100KB+300KB, and then a maintenance cost of maybe 20KB/hour. But actually, once we&#39;ve gotten the maintenance level so low, we should think about updating the consensus more often than once an hour. The goal would be to get relays that change IP addresses back into action as soon as possible -- currently it takes 2 to 4 hours before a new relay (or a relay with a new location) gets noticed by clients. Since <a href="http://freehaven.net/%7Ekarsten/metrics/dirarch-2009-02-11.pdf">one-third of Tor relays run on dynamic IP addresses</a>, bringing that level down to 30 to 60 minutes could mean a lot more network capacity.</p>

<p>Down the road, there are even more radical design changes to consider. One day there will be too many relays for every client to know about all of them, so we will want to partition the network (see Section 4.4 of <a href="https://svn.torproject.org/svn/projects/roadmaps/2008-12-19-roadmap-full.pdf">Tor&#39;s development roadmap</a>). When we do that, we can bound the amount of directory info that each client has to maintain. Another promising idea is to figure out ways to let clients build paths through the network while requiring less information about each relay. There&#39;s definitely a tradeoff here between centralized coordination (which is easier to design, and can more easily provide good anonymity properties) and scaling to many tens of thousands of relays. But that, as they say, is a story for another time.</p>

</div>
<p>- arma</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
