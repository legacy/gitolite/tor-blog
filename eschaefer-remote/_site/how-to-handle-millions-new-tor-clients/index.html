<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>How to handle millions of new Tor clients</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>How to handle millions of new Tor clients</h2>
<p class="meta">05 Sep 2013</p>

<div class="post">
<p>[<strong>tl;dr</strong> : if you want your Tor to be more stable, upgrade to a Tor Browser Bundle with Tor 0.2.4.x in it, and then wait for enough relays to upgrade to today&#39;s 0.2.4.17-rc release.]</p>

<p>Starting around August 20, we started to see a sudden spike in the number of Tor clients. By now it&#39;s unmistakable: there are millions of new Tor clients and the numbers continue to rise:</p>

<p><img src="https://people.torproject.org/%7Earma/direct-users-2013-07-07-2013-09-04.png" alt="Tor users in summer 2013"></p>

<p>Where do these new users come from? My current best answer is a botnet.</p>

<p>Some people have speculated that the growth in users comes from activists in Syria, Russia, the United States, or some other country that has good reason to have activists and journalists adopting Tor en masse lately. Others have speculated that it&#39;s due to massive adoption of the Pirate Browser (a Tor Browser Bundle fork that discards most of Tor&#39;s security and privacy features), but we&#39;ve talked to the Pirate Browser people and the downloads they&#39;ve seen can&#39;t account for this growth. The fact is, with a growth curve like this one, there&#39;s basically no way that there&#39;s a new human behind each of these new Tor clients. These Tor clients got bundled into some new software which got installed onto millions of computers pretty much overnight. Since no large software or operating system vendors have come forward to tell us they just bundled Tor with all their users, that leaves me with one conclusion: somebody out there infected millions of computers and as part of their plan they installed Tor clients on them.</p>

<p>It doesn&#39;t look like the new clients are using the Tor network to send traffic to external destinations (like websites). Early indications are that they&#39;re accessing hidden services — fast relays see &quot;Received an ESTABLISH_RENDEZVOUS request&quot; many times a second in their info-level logs, but fast exit relays don&#39;t report a significant growth in exit traffic. One plausible explanation (assuming it is indeed a botnet) is that it&#39;s running its <a href="http://en.wikipedia.org/wiki/Botnet#Organization">Command and Control (C&amp;C)</a> point as a hidden service.</p>

<p>My first observation is &quot;holy cow, the network is still working.&quot; I guess all that work we&#39;ve been doing on scalability was a good idea. The second observation is that these new clients actually aren&#39;t adding that much traffic to the network. Most of the pain we&#39;re seeing is from all the new circuits they&#39;re making — Tor clients build circuits preemptively, and millions of Tor clients means millions of circuits. Each circuit requires the relays to do expensive public key operations, and many of our relays are now maxed out on CPU load.</p>

<p>There&#39;s a possible dangerous cycle here: when a client tries to build a circuit but it fails, it tries again. So if relays are so overwhelmed that they each drop half the requests they get, then more than half the attempted circuits will fail (since all the relays on the circuit have to succeed), generating even more circuit requests.</p>

<p>So, how do we survive in the face of millions of new clients?</p>

<p>Step one was to see if there was some simple way to distinguish them from other clients, like checking <a href="https://trac.torproject.org/projects/tor/ticket/9653">if they&#39;re using an old version of Tor</a>, and have entry nodes refuse connections from them. Alas, it looks like they&#39;re running 0.2.3.x, which is the current recommended stable.</p>

<p>Step two is to get more users using the <a href="https://gitweb.torproject.org/tor.git/blob/refs/tags/tor-0.2.4.17-rc:/ChangeLog#l769">NTor circuit-level handshake</a>, which is new in Tor 0.2.4 and offers stronger security with lower processing overhead (and thus less pain to relays). Tor 0.2.4.17-rc comes with an added twist: we <a href="https://trac.torproject.org/projects/tor/ticket/9574">prioritize</a> NTor create cells over the old <a href="http://freehaven.net/anonbib/date.html#tap:pet2006">TAP</a> create cells that 0.2.3 clients send, which a) means relays will get the cheap computations out of the way first so they&#39;re more likely to succeed, and b) means that Tor 0.2.4 users will jump the queue ahead of the botnet requests. The Tor 0.2.4.17-rc release also comes with some new log messages to <a href="https://trac.torproject.org/projects/tor/ticket/9658">help relay operators track how many of each handshake type they&#39;re handling</a>.</p>

<p>(There&#39;s some tricky calculus to be done here around whether the botnet operator will upgrade his bots in response. Nobody knows for sure. But hopefully not for a while, and in any case the new handshake is a lot cheaper so it would still be a win.)</p>

<p>Step three is to temporarily disable some of the client-side performance features that build extra circuits. In particular, our <a href="https://gitweb.torproject.org/tor.git/blob/tor-0.2.4.17-rc:/ReleaseNotes#l1764">circuit build timeout</a> feature estimates network performance for each user individually, so we can tune which circuits we use and which we discard. First, in a world where successful circuits are rare, discarding some — even the slow ones — might be unwise. Second, to arrive at a good estimate faster, clients make a series of throwaway measurement circuits. And if the network is ever flaky enough, clients discard that estimate and go back and measure it again. These are all fine approaches in a network where most relays can handle traffic well; but they can contribute to the above vicious cycle in an overloaded network. The next step is to <a href="https://trac.torproject.org/projects/tor/ticket/9670">slow down these exploratory circuits</a> in order to reduce the load on the network. (We would temporarily disable the circuit build timeout feature entirely, but it turns out we had a <a href="https://trac.torproject.org/projects/tor/ticket/9671">bug where things get worse in that case</a>.)</p>

<p>Step four is longer-term: there remain some <a href="https://trac.torproject.org/projects/tor/ticket/9662">NTor handshake performance improvements</a> that will make them faster still. It would be nice to get circuit handshakes on the relay side to be really cheap; but it&#39;s an open research question how close we can get to that goal while still providing strong handshake security.</p>

<p>Of course, the above steps aim only to get our head back above water for this particular incident. For the future we&#39;ll need to explore further options. For example, we could rate-limit circuit create requests at entry guards. Or we could learn to recognize the circuit building signature of a bot client (maybe it triggers a new hidden service rendezvous every n minutes) and refuse or <a href="http://en.wikipedia.org/wiki/Tarpit_%28networking%29">tarpit</a> connections from them. Maybe entry guards should demand that clients solve captchas before they can build more than a threshold of circuits. Maybe we rate limit TAP handshakes at the relays, so we leave more CPU available for other crypto operations like TLS and AES. Or maybe we should immediately refuse all TAP cells, effectively shutting 0.2.3 clients out of the network.</p>

<p>In parallel, it would be great if botnet researchers would identify the particular characteristics of the botnet and start looking at ways to shut it down (or at least get it off of Tor). Note that getting rid of the C&amp;C point may not really help, since it&#39;s the rendezvous attempts from the bots that are hurting so much.</p>

<p>And finally, I still maintain that if you have a multi-million node botnet, it&#39;s silly to try to hide it behind the 4000-relay Tor network. These people should be using their botnet as a peer-to-peer anonymity system for itself. So I interpret this incident as continued exploration by botnet developers to try to figure out what resources, services, and topologies integrate well for protecting botnet communications. Another facet of solving this problem long-term is helping them to understand that Tor isn&#39;t a great answer for their problem.</p>

</div>
<p>- arma</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
