---
layout: post
title: "Code Commit Movie of our Website"
permalink: code-commit-movie-our-website
date: 2011-05-08
author: phobos
category: blog
tags: ["art", "codeswarm", "svn", "tor website", "visualization"]
---

A visual history of our website, as seen through svn code commits, from the beginning.  
The full movie is at [https://media.torproject.org/video/tor-website-history-movie.mpg](https://media.torproject.org/video/tor-website-history-movie.mpg "https://media.torproject.org/video/tor-website-history-movie.mpg") or after the jump.

<center>
<br>
<video controls="controls" autoplay="autoplay"><br>
<source src="https://media.torproject.org/video/tor-website-history-movie.mpg"><br>
If there is no movie here, click the above link.</source></video><br>
</center>
