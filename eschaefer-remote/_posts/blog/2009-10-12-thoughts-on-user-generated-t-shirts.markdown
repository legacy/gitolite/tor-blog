---
layout: post
title: "Thoughts on user generated t-shirts?"
permalink: thoughts-on-user-generated-t-shirts
date: 2009-10-12
author: phobos
category: blog
tags: ["activists", "new designs", "t-shirts", "user generated"]
---

Some activists in China came up with this design for tor t-shirts. Comment if you think we should make and sell these for $20/ea.

<center><a href="http://freehaven.net/~phobos/2009-09-29-tshirt.jpg"><img src="http://freehaven.net/~phobos/2009-09-29-tshirt.jpg" width="320" height="140"></a></center>

<center>(click the image for full size)</center>

If you have other ideas, feel free to link to an image of them!

