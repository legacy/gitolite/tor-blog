---
layout: post
title: "Picturing Tor censorship in China"
permalink: picturing-tor-censorship-china
date: 2009-10-14
author: phobos
category: blog
tags: ["bridges", "censorship", "censorship circumvention", "china", "directory requests", "graphs", "great firewall of china"]
---

As [reported](https://blog.torproject.org/blog/tor-partially-blocked-china), Tor was partially blocked by China on September 25th or so in anticipation of the CCP October 1, 2009 60th anniversary.

Here's what one directory mirror recorded for September,

![](https://blog.torproject.org/files/2009-10-12-china-dirreq-trusted.png)

And here's the growth of [bridge](https://www.torproject.org/bridges) users in response. Alas, like our graphs of [bridge use in Iran in June 2009](https://blog.torproject.org/blog/measuring-tor-and-iran-part-two), we only have relative counts for bridge use, not absolute counts. But with a 70x increase in a week, we are talking about 10000+ bridge users:

![](https://blog.torproject.org/files/2009-10-12-china-bridge-usage.png)

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/2009-10-12-china-dirreq-trusted.png">2009-10-12-china-dirreq-trusted.png</a></td>
<td>16.93 KB</td> </tr>
 <tr class="even">
<td><a href="https://blog.torproject.org/files/2009-10-12-china-bridge-usage.png">2009-10-12-china-bridge-usage.png</a></td>
<td>16.51 KB</td> </tr>
</tbody>

