---
layout: post
title: "June 2011 Progress Report"
permalink: june-2011-progress-report
date: 2011-07-13
author: phobos
category: blog
tags: ["advocacy", "anonymity advocacy", "progress report", "tor releases", "updates"]
---

The June 2011 progress report is at the bottom of this post and at [https://blog.torproject.org/files/2011-June-Monthly-Report.pdf](https://blog.torproject.org/files/2011-June-Monthly-Report.pdf "https://blog.torproject.org/files/2011-June-Monthly-Report.pdf").

Highlights include ECC improvements, updated translations, software releases, arm progress, vidalia updates, and thandy progress.

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/2011-June-Monthly-Report.pdf">2011-June-Monthly-Report.pdf</a></td>
<td>650.73 KB</td> </tr>
</tbody>

